import { terser } from "rollup-plugin-terser";

export default {
    input: "index.mjs",
    output: {
        file: "dist/index.mjs",
        format: "esm"
    },
    plugins: [
        process.env.BUILD === "production" && terser()
    ],
    external: [
        "fluent",
        "logging",
        "trusted-types",
        "@quotquot/element"
    ]
};
