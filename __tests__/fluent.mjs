import { FluentBundle, FluentResource } from "@fluent/bundle";

export default {
    getBundleResolver: resourceId => ({
        getBundle: lang => {
            const ftl = `key = ${lang}/key\n\nkey2 = ${lang}/key2<span>foo</span>`;
            const resource = new FluentResource(ftl);
            const bundle = new FluentBundle(lang);
            bundle.addResource(resource);
            return bundle;
        }
    })
};
