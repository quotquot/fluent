/* eslint require-jsdoc: off */
/* eslint func-style: off */
/* eslint no-underscore-dangle: off */

import { State, RenderingManager } from "@quotquot/element/dist/index.mjs";
import FluentRenderer from "../index.mjs";

test("Create fluent-test element", async() => {

    const state = new State();

    const renderer = new FluentRenderer();
    await renderer.setup(document.body, state);

    const manager = new RenderingManager();
    await manager.setup(document.body, state);
    manager.addRenderer(renderer);

    const template = document.createElement("template");
    template.innerHTML = '<div data-l10n-id="key"></div>';
    await manager.appendTemplate(template, state.getAll());
    expect(document.body.innerHTML).toBe('<div data-l10n-id="key">en-US/key</div>');

    const div = document.body.firstElementChild;

    template.innerHTML = template.innerHTML.replaceAll("key", "key2");
    manager.clear();
    await manager.appendTemplate(template, state.getAll());
    expect(document.body.innerHTML).toBe('<div data-l10n-id="key2">en-US/key2<span>foo</span></div>');

    await state.update({ language: "fr-FR" });
    expect(document.body.innerHTML).toBe('<div data-l10n-id="key2">fr-FR/key2<span>foo</span></div>');

    template.innerHTML = template.innerHTML.replaceAll(' data-l10n-id="key2"', "");
    manager.clear();
    await manager.appendTemplate(template, state.getAll());
    expect(document.body.innerHTML).toBe("<div></div>");

    // TODO: check that attr.handler is removed and the element cleared. Calling either:
    // expect(attr.handler).toBeFalsy();
    // expect(el.shadowRoot.innerHTML).toBe("<div></div>");
    // fails because the mutation observer has not been run yet.

    manager.clear();
    await manager.cleanup(document.body, state);
    await renderer.cleanup(document.body, state);
    expect(document.body.innerHTML).toBe("");
});
