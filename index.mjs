/*
 * Copyright 2020-2021 Johnny Accot
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by the
 * European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *      http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import fluent from "fluent";
import logging from "logging";
import policy from "trusted-types";
import { AttrRenderer, BaseHandler } from "@quotquot/element";

const logger = logging.getLogger("@quotquot/fluent");

const XHTML_NAMESPACE_URI = document.documentElement.namespaceURI;

class FluentHandler  extends BaseHandler {

    static #L10NID_ATTR_NAME = "data-l10n-id";
    static #L10NARGS_ATTR_NAME = "data-l10n-args";
    static #L10NDEPS_ATTR_NAME = "data-l10n-deps";

    static getAttributeNodes(el) {
        const attrs = [];
        for (const attrName of [
            FluentHandler.#L10NID_ATTR_NAME,
            FluentHandler.#L10NARGS_ATTR_NAME,
            FluentHandler.#L10NDEPS_ATTR_NAME
        ]) {
            const attr = el.getAttributeNode(attrName);
            if (attr)
                attrs.push(attr);
        }
        return attrs.length ? attrs : null;
    }

    #deps;
    #el;
    #key;
    #nodes;
    #params;
    #renderer;

    constructor(renderer, el, parent, attrs) {
        super(parent);
        this.#renderer = renderer;
        this.#el = el;
        this.#nodes = [];
        for (const { name, value } of attrs) {
            switch (name) {
                case FluentHandler.#L10NID_ATTR_NAME:
                    this.#key = value.trim();
                    break;
                case FluentHandler.#L10NARGS_ATTR_NAME:
                    this.#params = JSON.parse(value || "{}");
                    break;
                case FluentHandler.#L10NDEPS_ATTR_NAME:
                    this.#deps = new Set(value.split(" "));
                    break;
                default:
                    throw new Error(`Unexpected attribute "${name}"`);
            }
        }
    }

    async #render(manager, newState) {
        const values = this.getValues(newState);
        this.#nodes = this.#renderer.translate(this.#el, this.#key, this.#params, values);
    }

    async render(manager, newState, context) {
        this.setContext(context);
        await this.#render(manager, newState);
    }

    async update(manager, oldState, newState, changed) {
        if (changed.has("language") || (this.#deps && !changed.isDisjointFrom(this.#deps))) {
            this.clear();
            this.#render(manager, newState);
        }
    }

    clear() {
        for (const node of this.#nodes)
            node.remove();
        this.#nodes.length = 0;
    }
}

export default class FluentRenderer extends AttrRenderer {

    static #MARKUP_REGEXP = /<|&#?\w+;/;

    #resourceId;
    #resolver;
    #update;
    #bundle;

    constructor() {
        super(FluentHandler);
    }

    async #resolve(language) {
        this.#bundle = await this.#resolver.getBundle(language);
        if (!this.#bundle)
            throw new Error(`missing ${this.#resourceId} bundle for ${language}`);
        return language;
    }

    translate(el, key, params, values) {
        // TODO: merge params with values
        // https://projectfluent.org/dom-l10n-documentation/l10n-args.html
        const { value, attributes } = this.#bundle.getMessage(key);
        const nodes = [];
        if (value) {
            const translation = this.#bundle.formatPattern(value, values);
            if (FluentRenderer.#MARKUP_REGEXP.test(value) && !(
                el.localName === "title" && el.namespaceURI === XHTML_NAMESPACE_URI
            )) {
                el.innerHTML = policy.createHTML(translation);
                nodes.push(...el.childNodes);
            }
            else {
                const textNode = document.createTextNode(translation);
                el.appendChild(textNode);
                nodes.push(textNode);
            }
        }
        for (let [name, attr] of Object.entries(attributes)) {
            const attrNode = document.createAttribute(name);
            attrNode.value = this.#bundle.formatPattern(attr, values);
            el.setAttributeNode(attrNode);
            nodes.push(attrNode);
        }
        return nodes;
    }

    async setup(root, state) {
        super.setup(root, state);
        let language = state.get("language");
        if (!language) {
            language = navigator.language;
            state.set("language", language);
        }
        // TODO: correct resourceId for non shadow roots (document or non custom el)
        const tagName = root instanceof ShadowRoot ? root.host.tagName : root.tagName;
        this.#resourceId = tagName.toLowerCase();
        this.#resolver = fluent.getBundleResolver(this.#resourceId);
        if (!this.#resolver)
            throw new Error(`no bundle resolver for ${this.#resourceId}`);
        this.#update = () => state.update({ language: navigator.language }).catch(logger.error);
        window.addEventListener("languagechange", this.#update);
        await this.#resolve(language);
    }

    async cleanup(root, state) {
        super.cleanup(root, state);
        window.removeEventListener("languagechange", this.#update);
        this.#update = null;
    }

    async onBeforeUpdate(root, state, newState, changed) {
        if (changed.has("language"))
            await this.#resolve(newState.language);
    }
}
